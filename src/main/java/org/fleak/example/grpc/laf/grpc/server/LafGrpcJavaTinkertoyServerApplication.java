package org.fleak.example.grpc.laf.grpc.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LafGrpcJavaTinkertoyServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LafGrpcJavaTinkertoyServerApplication.class, args);
	}

}
