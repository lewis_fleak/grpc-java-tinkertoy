package org.fleak.example.grpc.laf.grpc.server.listener;

import com.google.protobuf.Timestamp;
import io.grpc.stub.StreamObserver;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;
import org.fleak.example.grpc.laf.grpc.server.HelloWorldGreeting;
import org.fleak.example.grpc.laf.grpc.server.HelloWorldResponse;
import org.fleak.example.grpc.laf.grpc.server.HelloWorldServiceGrpc;

import java.net.InetAddress;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

@Slf4j
@GrpcService
public class HelloWorldServiceImplementation extends HelloWorldServiceGrpc.HelloWorldServiceImplBase {

    private static final String RESPONSE_MESSAGE_FORMAT =
            "Hail! %s asks you live long and prosper %s %s%s";

    private String hostName;

    @SneakyThrows
    private String fetchHostName() {
        return hostName != null ? hostName : (hostName = InetAddress.getLocalHost().getHostName());
    }

    private static String formatResponseMessage(HelloWorldGreeting input, String host, String punctuation) {
        return String.format(RESPONSE_MESSAGE_FORMAT, host, input.getFirstName().trim(), input.getLastName().trim(), punctuation);
    }

    @SneakyThrows
    @Override
    public void hail(HelloWorldGreeting request, StreamObserver<HelloWorldResponse> responseObserver) {
        // Get start time
        Instant start = Instant.now();

        log.info("Received gRPC request:  {}", request);

        // build response message object
        HelloWorldResponse response =
                HelloWorldResponse.newBuilder()
                        .setResponse(
                                formatResponseMessage(request, fetchHostName().toUpperCase(),".").trim()
                        )
                        .setResponseTime(
                                Timestamp.newBuilder()
                                        .setSeconds(start.getEpochSecond())
                                        .setNanos(start.getNano())
                                        .build()
                        ).build();

        // sending the response
        responseObserver.onNext(response);
        // signaling no further responses are coming
        responseObserver.onCompleted();

        // calculating and logging out elapsed time
        Instant end = Instant.now();
        Duration elapsedTime = Duration.between(start, end);
        log.info("Server Elapsed Time:  {} microseconds", TimeUnit.NANOSECONDS.toMicros(elapsedTime.getNano()));
    }
}
